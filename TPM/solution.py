import socket
import enum
import string

# https://gist.github.com/leonjza/f35a7252babdf77c8421 
class Netcat:
   """ Python 'netcat like' module """
   def __init__(self, ip, port):
      self.buff = b""
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.socket.connect((ip, port))

   def read(self, length = 4096):
      """ Read 1024 bytes off the socket """
      return self.socket.recv(length)

   def read_until(self, data):
      """ Read data into the buffer until we have data """
      while not data in self.buff:
         self.buff += self.socket.recv(1024)
      pos = self.buff.find(data)
      rval = self.buff[:pos + len(data)]
      self.buff = self.buff[pos + len(data):]
      return rval

   def write(self, data):
      self.socket.send(data)
   
   def close(self):
      self.socket.close()

class TPMApi(object):
   class Commands(enum.Enum):
      LOAD_KEY  = 0
      ENCRYPT   = 1
      DECRYPT   = 2
      RESET_KEY = 3
      EXIT      = 4
   
   def __init__(self, server, port) -> None:
      self.nc = Netcat(server, port)
      self.read_output()
      
   def read_output(self, silent = False):
      s = self.nc.read(4096).decode("utf-8") 
      if not silent:
         print(s)
      return s
   
   def send_payload(self, payload, response = True, silent = True):
      r = None
      self.nc.write(str.encode(f"{payload}\n"))
      if response:
         r = self.read_output(silent)
      return r

   def send_command(self, command : Commands, response = True, silent = True):
      return self.send_payload(command.value + 1, response, silent)

   def load_key(self, key):
      self.send_command(self.Commands.LOAD_KEY)
      res = self.send_payload(key)
      return res.split("\n")[0]
   
   def encrypt(self, plaintext):
      self.send_command(self.Commands.ENCRYPT)
      res = self.send_payload(plaintext)
      return res.split("\n")[0]

   def decrypt(self, chiper):
      self.send_command(self.Commands.DECRYPT)
      res = self.send_payload(chiper)
      return res.split("\n")[0]
   
   def reset_key(self):
      self.send_command(self.Commands.RESET_KEY)
   
   def exit(self):
      self.send_command(self.Commands.EXIT)

def find_part_of_key(key, num_of_bytes):
   s = string.printable
   for x in s:
      suspected_part_of_key = f"{key}{ord(x):02X}"

      # Load the suspected part of key 
      print(f"[-] Suspected part of key = {suspected_part_of_key}")
      tpm_api.load_key(f"{suspected_part_of_key}")

      # Encrypt with the suspected part of key 
      suspected_chiper = tpm_api.encrypt(textplain)
      print(f"[-] Suspected chiper = {suspected_chiper}")

      # Comapre the real and suspected chipers
      if real_enc == suspected_chiper:
         print(f"[-] Part of key found {suspected_part_of_key}")
         break
   return suspected_part_of_key

if __name__ == "__main__":
   #tpm_api = TPMApi("127.0.0.1", 3800)
   tpm_api = TPMApi("tpm.ichsa.ctf.today", 8002)

   # Static textplain to encrypt
   textplain = "AA" * 16

   # Need to reset the key in order to get the real key 
   # to simulate the situation that someone might use it and modify it
   tpm_api.reset_key()

   # Encrypt the textplain in order to get the chiper of the real key and our static textplain
   real_enc = tpm_api.encrypt(textplain)
   print(real_enc)

   key = ""

   # Retrive the first 2 bytes due to the load key minimun bytes input
   #key = find_part_of_key(key, 2)

   # Retrive the next bytes one by one
   for i in range(1, 16):
      key = find_part_of_key(key, 1)

   print(f"[-] The flag is {bytes.fromhex(key).decode('utf-8')}")