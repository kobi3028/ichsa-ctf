from Crypto.Cipher import AES
from typing import List
import inspect
import secrets
import abc
from pyctfcommon import OptionPicker


BS = 16
pad = lambda s: s + (BS - len(s) % BS) * b"\00"
class Challenge(OptionPicker.OptionPicker):
   def __init__(self):
      super().__init__()
      self.key = bytearray(secrets.token_bytes(16))

   @OptionPicker.OptionPicker._option(help='Load custom key')
   def load_key(self):
      key_bytes = OptionPicker.InputHexBytes([OptionPicker.InputLengthValidation(1, 17)]).input("Enter hex bytes: ")
      for i, b in enumerate(key_bytes):
         self.key[i] = b
      print(f"{len(key_bytes)} bytes loaeded")

   @OptionPicker.OptionPicker._option(help='Encrypt user input')
   def encrypt(self):
      plaintext = OptionPicker.InputHexBytes().input("Enter hex bytes to encrypt: ")
      plaintext = pad(plaintext)
      chiper = AES.new(bytes(self.key), AES.MODE_ECB)
      print(chiper.encrypt(plaintext).hex())

   @OptionPicker.OptionPicker._option(help='Decrypt user input')
   def decrypt(self):
      plaintext = OptionPicker.InputHexBytes().input("Enter hex bytes to decrypt: ")
      plaintext = pad(plaintext)
      print(secrets.token_bytes(len(plaintext)).hex())
      
   @OptionPicker.OptionPicker._option(help='Reset the key')
   def reset_key(self):
      self.key = bytearray(b"TH3_TPM_K3Y_YAY!")
      with open("art.txt") as f:
         print(f.read())
         f.close()

def main():
   c = Challenge()
   c.run()
   
if __name__ == "__main__":
   try:
      main()
   except:
      print("Some error occured")