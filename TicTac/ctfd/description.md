TicTac

Our RSA cryptosystem is broken, it looks like each request use different unknown primes.

A lot of customers are using our RSA cryptosystem because of our fast modexp.

We must get the key to recover our costumers messages, can you help us recover the RSA key?

Connect: `nc {HOST} {PORT}`

HINT:
def modexp_lr(a, b, n):
   r = 1
   for bit in reversed(bits_of_n(b)):
      r = r * r % n
      if bit == 1:
         r = r * a % n
      print('.', end="", flush=True)
   print('')
   return r

challenge author: [Aviv Cohen (zVaz)](https://twitter.com/_zVaz_)