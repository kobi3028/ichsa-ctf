import time
from Crypto.Util import number
from pyctfcommon import OptionPicker

def bits_of_n(n):
   bits = []
   while n:
      bits.append(n % 2)
      n //= 2
   return bits

def modexp_lr(a, b, n):
   r = 1
   for bit in reversed(bits_of_n(b)):
      r = r * r % n
      if bit == 1:
         r = r * a % n
         time.sleep(0.1)
      print('.', end="", flush=True)
   print("")
   return r

class Challenge(OptionPicker.OptionPicker):
   def __init__(self):
      super().__init__()
      print("Initializing please wait...", flush=True)
      
      self.p = number.getPrime(2048)
      self.q = number.getPrime(2048)
      self.n = self.p * self.q
      d_bytes = b"You are on the right way keep going! The flag is: ICHSA_CTF{Alice:H0w l0ng 1s for3v3r? White R@bbit:S0met1mes, just one s3cond.}"
      self.d = int(d_bytes.hex(), 16)
   
   @OptionPicker.OptionPicker._option(help='Decrypt payload')
   def decrypt(self):
      m = OptionPicker.InputHexBytes([OptionPicker.InputLengthValidation(2, 128)]).input("Enter hex bytes: ")
      m_int = int(m.hex(), 16)
      print(f'plaintext = {modexp_lr(m_int, self.d, self.n):X}')

def main():
   c = Challenge()
   c.run()
   
if __name__ == "__main__":
   try:
      main()
   except:
      print("Some error occured")