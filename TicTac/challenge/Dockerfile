FROM python:3.7.8-slim as build

RUN mkdir -p /app
ADD pyctfcommon /app/pyctfcommon
WORKDIR /app/pyctfcommon

RUN python3 -m pip install --upgrade build
RUN python3 -m pip install --upgrade setuptools
RUN python3 -m build

# ----------------------------------------------

FROM python:3.7.8-slim

RUN mkdir -p /app

COPY --from=build /app/pyctfcommon/dist/*.whl /app

COPY TicTac/challenge/tictac_challenge.py /app
COPY TicTac/challenge/requirements.txt /app
WORKDIR /app

RUN apt-get update && apt-get install -y socat

RUN pip3 install -r /app/requirements.txt
RUN pip3 install --no-index --no-deps /app/*.whl

# Set non root user
RUN useradd -c 'User' -m -d /home/user -s /bin/bash user
RUN chown -R user:user /home/user

USER user
ENV HOME /home/user

EXPOSE 3780
CMD ["socat", "TCP-LISTEN:3810,reuseaddr,fork", "EXEC:'python /app/tictac_challenge.py'"]