import socket
import enum
import time

# https://gist.github.com/leonjza/f35a7252babdf77c8421 
class Netcat:
   """ Python 'netcat like' module """
   def __init__(self, ip, port):
      self.buff = b""
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.socket.connect((ip, port))

   def read(self, length = 4096):
      """ Read 1024 bytes off the socket """
      return self.socket.recv(length)

   def read_until(self, data):
      """ Read data into the buffer until we have data """
      while not data in self.buff:
         self.buff += self.socket.recv(1024)
      pos = self.buff.find(data)
      rval = self.buff[:pos + len(data)]
      self.buff = self.buff[pos + len(data):]
      return rval

   def write(self, data):
      self.socket.send(data)
   
   def close(self):
      self.socket.close()

#nc = Netcat("127.0.0.1", 3810)
nc = Netcat("tictac.ichsa.ctf.today", 8009)

print(nc.read(4096))

nc.write(b"1\n")
print(nc.read(4096))
nc.write(b"AAAA\n")

time_list = []

time_list.append(time.time())
while nc.read(1) == b".":
   time_list.append(time.time())


delta_time_list = []
for i in range(0, len(time_list)-1):
   delta_time_list.append(time_list[i+1] - time_list[i])

print(delta_time_list)

avg = sum(delta_time_list)/len(delta_time_list)

binary_time_list = [1 if x > avg else 0 for x in delta_time_list]

num = 0
for b in binary_time_list:
    num = 2 * num + b

print(bytes.fromhex(f"{num:X}"))