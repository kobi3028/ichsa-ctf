import random
from z3 import *
import gates

def gen_password():
   outputs = []
   password = "101100100110111010100010"
   inputs = []
   gen_input = lambda gi, c: gates.NotGate(gi) if not c else gi
   password_zeros = []
   for i in range(0, 24, 1):
      inputs.append(gates.GateInput(input_name = f"{i}"))
      if password[i] == "0":
         password_zeros.append(i)

   for i in range(0, 23, 2):
      #random_not = lambda gi: gates.NotGate(gi) if random.getrandbits(1) else gi
      #outputs.append(gates.AndGate(random_not(gates.GateInput(input_name = f"I{i}")), random_not(gates.GateInput(input_name = f"I{i + 1}"))))
      if password[i] == "1" and password[i + 1] == "1":
         outputs.append(gates.AndGate(gates.OrGate(inputs[i], gates.OrGate(inputs[random.choice(password_zeros)], inputs[random.choice(password_zeros)])), inputs[i + 1]))
      else:
         g1 = gen_input(inputs[i], password[i] == "1")
         g2 = gen_input(inputs[i+1], password[i + 1] == "1")
         outputs.append(gates.AndGate(g1, g2))

   #print(outputs)

   while(len(outputs) != 1):
      temp_outputs = []
      last_elem = None
      if len(outputs) % 2 == 1:
         last_elem = outputs.pop()
      for a, b in zip(*[iter(outputs)]*2):
         temp_outputs.append(gates.AndGate(a, b))
      if last_elem is not None:
         temp_outputs.append(last_elem)
      outputs = temp_outputs

   gates_tree = gates.GateOutput(outputs[0])

   print(str(gates_tree))

   I = Bools(" ".join([f"I{i}" for i in range(24)]))
   s = Solver()
   s.add(eval(str(gates_tree)))
   if s.check() == sat:
      m = s.model()
      print("".join("1" if m[bit] else "0" for bit in I))

   print()

   def iterate_gates(gate, parent, callback):
      if gate.i1 is not None:
         iterate_gates(gate.i1, gate, callback)
      if gate.i2 is not None:
         iterate_gates(gate.i2, gate, callback)
      if parent is not None:
         callback(gate, parent)

   def iterate_all(gate, callback):
      callback(gate)
      if gate.i1 is not None:
         iterate_all(gate.i1, callback)
      if gate.i2 is not None:
         iterate_all(gate.i2, callback)

   def random_change_gate(gate, parent):
      if random.randint(0, 5) != 0:
         return

      new_gate = None
      if isinstance(gate, gates.AndGate):
         new_gate = gates.NotGate(gates.OrGate(gates.NotGate(gate.i1), gates.NotGate(gate.i2)))
      elif isinstance(gate, gates.OrGate):
         new_gate = gates.NotGate(gates.AndGate(gates.NotGate(gate.i1), gates.NotGate(gate.i2)))

      if new_gate is not None:
         if parent.i1 == gate:
            parent.i1 = new_gate
         else:
            parent.i2 = new_gate
      del gate

   def random_change_gate_not(gate, parent):
      if random.getrandbits(1):
         return

      new_gate = None
      if isinstance(gate, gates.NotGate):
         if random.getrandbits(1):
            new_gate = gates.NotGate(gates.AndGate(gate.i1, gate.i1))
         else:
            new_gate = gates.NotGate(gates.OrGate(gate.i1, gate.i1))

      if new_gate is not None:
         if parent.i1 == gate:
            parent.i1 = new_gate
         else:
            parent.i2 = new_gate
      del gate

   def not_gates_reduction(gate, parent):    
      if random.getrandbits(1):
         return

      new_gate = None
      if isinstance(gate, gates.NotGate) and isinstance(gate.i1, gates.NotGate):
         new_gate =  gate.i1.i1

      if new_gate is not None:
         if parent.i1 == gate:
            parent.i1 = new_gate
         else:
            parent.i2 = new_gate
      del gate


   iterate_gates(gates_tree, None, random_change_gate)
   iterate_gates(gates_tree, None, random_change_gate_not)
   iterate_gates(gates_tree, None, random_change_gate)
   iterate_gates(gates_tree, None, not_gates_reduction)
   iterate_gates(gates_tree, None, random_change_gate_not)


   print(str(gates_tree))

   I = Bools(" ".join([f"I{i}" for i in range(24)]))
   s = Solver()
   s.add(eval(str(gates_tree)))
   if s.check() == sat:
      m = s.model()
      print("".join("1" if m[bit] else "0" for bit in I))

   all_gates = inputs

   def add_gates_to_global_list(gate):
      if gate.type != gates.GateTypes.INPUT:
         all_gates.append(gate)

   iterate_all(gates_tree, add_gates_to_global_list)

   for i, gate in enumerate(all_gates):
      gate.set_id(i)

   with open("gates_binary.bin", "wb") as f:
      for gate in all_gates:
         f.write(gate.to_bytes())


def print_debug_gates():
   def iterate_all(gate, callback):
      callback(gate)
      if gate.i1 is not None:
         iterate_all(gate.i1, callback)
      if gate.i2 is not None:
         iterate_all(gate.i2, callback)

   i1 = gates.GateInput(input_name = f"I1")
   i2 = gates.GateInput(input_name = f"I2")
   i3 = gates.GateInput(input_name = f"I3")

   a = gates.AndGate(i1, i2)
   n = gates.NotGate(i3)
   o = gates.OrGate(a, n)

   out = gates.GateOutput(o)

   all_gates = []

   def add_gates_to_global_list(gate):
      all_gates.append(gate)

   iterate_all(out, add_gates_to_global_list)

   for i, gate in enumerate(all_gates):
      gate.set_id(i)

   b = ""
   for gate in all_gates:
      b += gate.to_bytes().hex()
   print(b)
   print(out)

if __name__ == "__main__":
   gen_password()
   #print_debug_gates()