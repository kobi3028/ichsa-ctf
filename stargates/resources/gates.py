import ctypes
import enum

class GateTypes(enum.Enum):
   INPUT  = 0
   NOT    = 1
   OR     = 2
   AND    = 3
   OUTPUT = 4

class GateC(ctypes.LittleEndianStructure):
   _pack_ = 1
   _fields_ = [
      ("id"  , ctypes.c_uint16),
      ("type", ctypes.c_uint16),
      ("in1" , ctypes.c_uint16),
      ("in2" , ctypes.c_uint16),
   ]

inputs = []

class GateInput(object):
   def __init__(self, i1 = None, i2 = None, input_name = None):
      self.input_name = input_name
      self.i1 = i1
      self.i2 = i2
      self.type = GateTypes.INPUT
      self.id = None

   def set_id(self, id):
      self.id = id

   def __str__(self):
      return f"I[{self.input_name}]"

   def to_bytes(self):
      gate_c = GateC()
      gate_c.id   = self.id
      gate_c.type = self.type.value
      gate_c.in1  = self.i1.id if self.i1 is not None else 0xFFFF
      gate_c.in2  = self.i2.id if self.i2 is not None else 0xFFFF
      return bytes(gate_c)

class GateOutput(GateInput):
   def __init__(self, i1 : GateInput = None):
      super().__init__(i1, None)
      self.type = GateTypes.OUTPUT
   
   def __str__(self):
      return f"{self.i1}"

class Gate(GateInput):
   def __init__(self, i1 : GateInput = None, i2: GateInput = None):
      super().__init__(i1, i2)
   
class NotGate(Gate):
   def __init__(self, i1 : GateInput = None):
      super().__init__(i1, None)
      self.type = GateTypes.NOT

   def __str__(self):
      return f"Not({self.i1})"

class AndGate(Gate):
   def __init__(self, i1 : GateInput = None, i2: GateInput = None):
      super().__init__(i1, i2)
      self.type = GateTypes.AND

   def __str__(self):
      return f"And({self.i1}, {self.i2})"
   
class OrGate(Gate):
   def __init__(self, i1 : GateInput = None, i2: GateInput = None):
      super().__init__(i1, i2)
      self.type = GateTypes.OR

   def __str__(self):
      return f"Or({self.i1}, {self.i2})"