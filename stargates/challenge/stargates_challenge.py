from typing import List

# In order run  locally uncomment the line above
# import os, sys
# curr_path = os.path.dirname(os.path.abspath(__file__))
# print(os.path.abspath(os.path.join(curr_path, "..", "..", "pyctfcommon", "src")))
# sys.path.append(os.path.join(curr_path, "..", "..", "pyctfcommon", "src"))

from pyctfcommon import OptionPicker

class Challenge(OptionPicker.OptionPicker):
   def __init__(self):
      super().__init__()
      self.password = "101100100110111010100010"

   @OptionPicker.OptionPicker._option(help='Login')
   def login(self):
      input_password = OptionPicker.InputBase([
                           OptionPicker.Input10Validation(),
                           OptionPicker.InputLengthValidation(len(self.password), len(self.password) + 1)
                       ]).input(f'Enter password: ')
      if input_password == self.password:
         print("ICHSA_CTF{TH3_AL1EnS_GAt3S_AR3_S@M3_@S_OUrS}")
      else:
         print("Incorrect password")

   @OptionPicker.OptionPicker._option(help='HW sanity check')
   def sanity_check(self):
      # Or(And(I1, I2), Not(I3))
      print("00 00 04 00 01 00 ff ff 01 00 02 00 02 00 05 00")
      print("02 00 03 00 03 00 04 00 03 00 00 00 ff ff ff ff")
      print("04 00 00 00 ff ff ff ff 05 00 01 00 06 00 ff ff")
      print("06 00 00 00 ff ff ff ff")
      inp = OptionPicker.InputBase([
                           OptionPicker.Input10Validation(),
                           OptionPicker.InputLengthValidation(3, 4)
                       ]).input(f'Enter input: ')
      i1, i2, i3 = [int(c) for c in inp]
      print(f"Result: {(i1 and i2) or (not i3)}")

def main():
   with open("art.txt") as f:
      print(f.read())
   c = Challenge()
   c.run()
   
if __name__ == "__main__":
   try:
      main()
   except:
      print("Some error occured")
