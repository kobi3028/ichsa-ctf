import os, sys
import socket
import enum
import time

curr_path = os.path.dirname(os.path.abspath(__file__))
resources_path = os.path.abspath(os.path.join(curr_path, "resources"))
sys.path.append(resources_path)
ctfd_path = os.path.abspath(os.path.join(curr_path, "ctfd"))

import gates
from z3 import *

class Netcat:
   """ Python 'netcat like' module """
   def __init__(self, ip, port):
      self.buff = b""
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.socket.connect((ip, port))

   def read(self, length = 4096):
      """ Read 1024 bytes off the socket """
      return self.socket.recv(length)

   def read_until(self, data):
      """ Read data into the buffer until we have data """
      while not data in self.buff:
         self.buff += self.socket.recv(1024)
      pos = self.buff.find(data)
      rval = self.buff[:pos + len(data)]
      self.buff = self.buff[pos + len(data):]
      return rval

   def write(self, data):
      self.socket.send(data)
   
   def close(self):
      self.socket.close()

all_c_gates = []
all_gates = {}

gates_dict_factory = {
   gates.GateTypes.INPUT.value : gates.GateInput,
   gates.GateTypes.NOT.value   : gates.NotGate,
   gates.GateTypes.AND.value   : gates.AndGate,
   gates.GateTypes.OR.value    : gates.OrGate,
   gates.GateTypes.OUTPUT.value: gates.GateOutput,
}

with open(os.path.join(ctfd_path, "payload.bin"), "rb") as f:
   gate_bytes = f.read(8)
   while gate_bytes:
      all_c_gates.append(gates.GateC.from_buffer_copy(gate_bytes))
      gate_bytes = f.read(8)

output = None

for c_gate in all_c_gates:
   gate = gates_dict_factory[c_gate.type]()
   gate.set_id(c_gate.id)
   all_gates[c_gate.id] = gate
   if gate.type == gates.GateTypes.OUTPUT:
      output = gate

for c_gate in all_c_gates:
   gate = all_gates[c_gate.id]
   if c_gate.in1 != 0xFFFF:
      gate.i1 = all_gates[c_gate.in1]
   if c_gate.in2 != 0xFFFF:
      gate.i2 = all_gates[c_gate.in2]

i = 0
for id, gate in all_gates.items():
   if gate.type == gates.GateTypes.INPUT:
      gate.input_name = f"I{i}"
      i+=1

#print(output)

i = 0
for id, gate in all_gates.items():
   if gate.type == gates.GateTypes.INPUT:
      gate.input_name = f"{i}"
      i+=1

print(str(output))

I = Bools(" ".join([f"I{i}" for i in range(24)]))
s = Solver()
s.add(eval(str(output)))
if s.check() == sat:
   m = s.model()
   s = "".join("1" if m[bit] else "0" for bit in I)
   print(s)

   nc = Netcat("stargates.ichsa.ctf.today", 8006)

   print(nc.read(4096))

   nc.write(b"1\n")
   print(nc.read(4096))
   nc.write(str.encode(s) + b"\n")
   time.sleep(1)
   print(nc.read(4096))