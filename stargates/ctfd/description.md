We found some Alien HW

You must help us find the password in order to save the world

Our specialists found 2 payloads:
1. Password validation payload (attached here)
2. HW sanity check payload (see our interactive HW access)

They also found that the payload structure is:
   struct data {
      uint16_t id;
      uint16_t type;
      uint16_t ???;
      uint16_t ???;
   }

Interactive HW access: `nc {HOST} {PORT}`

challenge author: [Aviv Cohen (zVaz)](https://twitter.com/_zVaz_)