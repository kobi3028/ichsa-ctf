from binascii import hexlify, unhexlify
from textwrap import wrap
from Crypto.Cipher import AES
from pwn import *

# read the flag
proc = process("python poodle1.py", shell=True)
enc_flag = proc.recvuntil(b">> ").decode().split("\n")
enc_flag_blocks = [unhexlify(x) for x in wrap(enc_flag[5], AES.block_size*2)]

dec_msg = b""
num_of_blocks = len(enc_flag_blocks)
for c in range(1,num_of_blocks): # go over all the blocks
    chunk = enc_flag_blocks[-c-1]
    next_chunk = enc_flag_blocks[-c]
    for k in range(1, AES.block_size+1): # go over all letters in each block
        for i in range(1, 128): # go over all ASCII chars for each letter
            temp_msg = (bytes([i]) + dec_msg[:k-1]).rjust(AES.block_size, b"\x00")
            guess_chunk = bytes([chunk[j] ^ temp_msg[j] ^ k for j in range(AES.block_size)])
            to_send = hexlify(guess_chunk + next_chunk)

            proc.sendline(to_send)
            info = proc.recvuntil(b">> ")
            if b"invalid padding" not in info:
                print(f"found! {bytes([i])}, dec_msg: {dec_msg}")
                dec_msg = bytes([i]) + dec_msg
                break
print(dec_msg)
proc.sendline(b"nothing")
proc.close()
