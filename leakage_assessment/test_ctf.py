import os
# To stop the tensorflow debugging annoying messages
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from keras.models import Sequential
from keras.layers import Dense, BatchNormalization
from keras.optimizers import RMSprop
import numpy as np
import pickle

def standard_model(seq_length, classes=2):
	"""
	    Model architecture
	"""
	model = Sequential()
	model.add(Dense(120, activation='relu', input_shape=(seq_length,)))
	model.add(BatchNormalization())
	model.add(Dense(90, activation='relu'))
	model.add(BatchNormalization())
	model.add(Dense(50, activation='relu'))
	model.add(BatchNormalization())

	# use a *softmax* activation for single-label classification
	# and *sigmoid* activation for multi-label classification
	model.add(Dense(classes, activation='softmax'))

	model.compile(loss='categorical_crossentropy', optimizer=RMSprop(lr=0.00001), metrics=['accuracy'])

	return model

def shuffle_traces(x0, y0, x1, y1):
    x = []
    y = []
    x_val = []
    y_val = []

    running_range = len(y1)
    if len(y0) < len(y1):
        running_range = len(y0)

    for i in range(running_range):
        if i > running_range * 0.7:
            x_val.append(x0[i])
            y_val.append(y0[i])
            x_val.append(x1[i])
            y_val.append(y1[i])
        else:
            x.append(x0[i])
            y.append(y0[i])
            x.append(x1[i])
            y.append(y1[i])

    return np.array(x), np.array(y), np.array(x_val), np.array(y_val)

subfolders = [f.path for f in os.scandir("data/") if f.is_dir()]
for subfolder in subfolders:
    group_0_path = subfolder + "/group_0.txt"
    group_1_path = subfolder + "/group_1.txt"

    with open(group_0_path, "rb") as fp:  # pickling
        group_0 = pickle.load(fp)
    labels_0 = [[0, 1] for _ in range(len(group_0))]

    with open(group_1_path, "rb") as fp:  # pickling
        group_1 = pickle.load(fp)
    labels_1 = [[1, 0] for _ in range(len(group_0))]

    X_train, Y_train, X_val, Y_val = shuffle_traces(group_0, labels_0, group_1, labels_1)

    model = standard_model(len(X_train[0]), classes=2)

    model.fit(X_train, Y_train, batch_size=16, epochs=500, verbose=0)

    if model.evaluate(X_val, Y_val, batch_size=16, verbose=0)[1] > 0.9:
        print(subfolder)