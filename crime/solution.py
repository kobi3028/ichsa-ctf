import base64
import socket
import sys

# https://gist.github.com/leonjza/f35a7252babdf77c8421 
class Netcat:
   """ Python 'netcat like' module """
   def __init__(self, ip, port):
      self.buff = b""
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.socket.connect((ip, port))

   def read(self, length = 4096):
      """ Read 1024 bytes off the socket """
      return self.socket.recv(length)

   def read_until(self, data):
      """ Read data into the buffer until we have data """
      while not data in self.buff:
         self.buff += self.socket.recv(1024)
      pos = self.buff.find(data)
      rval = self.buff[:pos + len(data)]
      self.buff = self.buff[pos + len(data):]
      return rval

   def write(self, data):
      self.socket.send(data)
   
   def close(self):
      self.socket.close()



class Crime(object):
    def __init__(self, ip, port):
        self.nc = Netcat(ip, port)
        self.eat_to_input()
    
    def eat_to_input(self):
        self.nc.read_until(b"Your input: ")
    
    def exit(self):
        self.nc.write(b"\n")
        self.nc.read_until(b"Bye!\n")
        self.nc.close()
        
    def get_len(self, guess):
        self.nc.write(guess+b"\n")
        r = self.nc.read_until(b"\n")[:-1]
        self.eat_to_input()
        return len(base64.b64decode(r))


def get_len(crime, guess, rounds=10):
    return int(sum(crime.get_len(guess) for _ in range(rounds))/rounds)

if __name__ == "__main__":
    crime = Crime("127.0.0.1", 3535)
    prefix = b"ICHSA_CTF{"
    L = len(prefix)
    olen = get_len(crime, prefix)
    print(prefix)
    alphabet = b"abcdefghijklmnopqrstuvwxyz_}"
    count = 0
    while True:
        l = 0
        for c in alphabet:
            l = get_len(crime, prefix + bytes([c]))
            if l == olen:
                prefix = prefix + bytes([c])
                if prefix[-1:] == b"}":
                    print(f'found: {prefix}')
                    sys.exit(0)
                print(count, prefix)
                break
        if L == len(prefix):
            count = count + 1
            if count > 100:
                print('failed')
                sys.exit(1)
        else:
            count = 0
            L = len(prefix)
