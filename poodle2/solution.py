from binascii import hexlify, unhexlify
from textwrap import wrap
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
from pwn import *

proc = process("python poodle2.py", shell=True)
# get the word to encrypt
word_to_send = proc.readline_startswith(b"just send me the word: ").split(b": ")[-1].strip()
# separate it to blocks
word_to_send = [x.encode() for x in wrap(pad(word_to_send, AES.block_size).decode(), AES.block_size)]

enc_msg = b"A" * AES.block_size
num_of_blocks = len(word_to_send)
for c in range(1,num_of_blocks+1): # go over all the blocks
    chunk = word_to_send[-c]
    assert len(enc_msg) % AES.block_size == 0, "should not reach here"
    next_chunk = enc_msg[:AES.block_size]
    for k in range(1, AES.block_size+1): # go over all letters in each block
        for i in range(0, 256): # go over all the possile values in a byte
            temp_msg = (bytes([i]) + enc_msg[:k-1]).rjust(AES.block_size, b"\x00")
            guess_chunk = bytes([chunk[j] ^ temp_msg[j] ^ k for j in range(AES.block_size)])
            to_send = hexlify(guess_chunk + next_chunk)

            proc.sendlineafter(b">> ", to_send)
            info = proc.recvline()
            if b"invalid padding" not in info:
                print(f"found! {bytes([i])}, enc_msg: {enc_msg}")
                enc_msg = bytes([i]) + enc_msg
                break
proc.sendlineafter(b">> ", hexlify(enc_msg))
print(proc.recvuntil(b">> "))
proc.sendline(b"nothing")
proc.close()
