from itertools import product
from hashlib import sha256
import base64
import socket
import sys
from string import digits, ascii_letters

# https://gist.github.com/leonjza/f35a7252babdf77c8421 
class Netcat:
   """ Python 'netcat like' module """
   def __init__(self, ip, port):
      self.buff = b""
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.socket.connect((ip, port))

   def read(self, length = 4096):
      """ Read 1024 bytes off the socket """
      return self.socket.recv(length)

   def read_until(self, data):
      """ Read data into the buffer until we have data """
      while not data in self.buff:
         self.buff += self.socket.recv(1024)
      pos = self.buff.find(data)
      rval = self.buff[:pos + len(data)]
      self.buff = self.buff[pos + len(data):]
      return rval

   def write(self, data):
      self.socket.sendall(data)
   
   def close(self):
      self.socket.close()



class HungryLattice(object):
    def __init__(self, ip, port):
        self.nc = Netcat(ip, port)
        self.get_public_key()
        self.eat_to_cipher_suit()
    
    def get_public_key(self):
        self.nc.read_until(b"NIST384p: ")
        self.pubkey = self.nc.read_until(b"\n")[:-1]
    
    def eat_to_cipher_suit(self):
        self.nc.read_until(b"sha256): ")

    def get_pow(self):
        self.nc.read_until(b"??? | ")
        nonce = self.nc.read_until(b')')[:-1]
        self.nc.read_until(b" == ")
        digest = self.nc.read_until(b'\n')[:-1]
        return nonce, digest

    def eat_to_pow(self):
        self.nc.read_until(b"??? is: ")

    def exit(self):
        self.nc.close()
    
    def solve_pow(self, nonce, digest):
        for secret in product((digits + ascii_letters).encode(), repeat=4):
            if sha256(bytes(secret) + nonce).digest() == digest:
                return bytes(secret)
        raise Exception('Failed to solve pow')

    def get_signature(self):
        self.nc.write(b"nist384p-sha256\n")
        nonce, digest = self.get_pow()
        secret = self.solve_pow(nonce, bytes.fromhex(digest.decode()))
        self.eat_to_pow()
        self.nc.write(secret+b'\n')
        self.nc.read_until(b'signature: (')
        r = self.nc.read_until(b', ')[:-2]
        s = self.nc.read_until(b')')[:-1]
        return (int(r), int(s))


def get_len(crime, guess, rounds=5):
    return int(sum(crime.get_len(guess) for _ in range(rounds))/rounds)

#remote = ("127.0.0.1", 3537)
remote = ("hungry_lattice.ichsa.ctf.today", 8005)

if __name__ == "__main__":
    hl = HungryLattice(*remote)
    #print(hl.pubkey.decode())
    for _ in range(6):
        r, s = hl.get_signature()
        print(hex(r), hex(s))
        hl.eat_to_cipher_suit()
# Afterward just use the TMP-FAIL sciprts as a basis to solve the lattice.
# See a working example in lattice_attack.py (SageMath is needed)
