from hashlib import sha256, sha384
from random import choice
from string import digits, ascii_letters

from ecdsa.curves import NIST384p, NIST256p
from ecdsa.keys import SigningKey
from ecdsa.util import randrange


alphabet = (digits + ascii_letters).encode()
def random(len):
    return bytes(choice(alphabet) for _ in range(len))

class PoW(object):
    def __init__(self, lvl=4, nonce_size=16, hashFn=sha256):
        self.hashFn = hashFn
        self.lvl = lvl
        self.nonce = random(nonce_size)
    
    def generate_tag(self, prefix):
        return self.hashFn(prefix+self.nonce).digest()
    
    def generate(self):
        return self.nonce, self.generate_tag(random(self.lvl))
    
    def validate(self, secret, digest):
        return digest == self.generate_tag(secret)


curves = {
    'nist384p': NIST384p,
    'nist256p': NIST256p
}

hashes = {
    'sha384': sha384,
    'sha256': sha256
}

def render_point(p):
    return f'({p.x()}, {p.y()})'

msg = b'Best Hacker Ever'

def main():
    sk3 = SigningKey.from_string(b"ICHSA_CTF{bias_is_the_best_food_for_pet_lattice}", curve=NIST384p, hashfunc=sha384)
    sk2 = SigningKey.from_string(b"BAD_ENTROPYFu\x04\x19'M\x02\x9el\xeb\xfe\xff\x91\x04\xd13\xbe\xfa\xd7\xbe\xc1", curve=NIST256p, hashfunc=sha256)
    print("Our Public-Keys:")
    print(f"\tNIST384p: {render_point(sk3.verifying_key.pubkey.point)}")
    print(f"\tNIST256p: {render_point(sk2.verifying_key.pubkey.point)}")
    while True:
        print(f"\nU can retrive your `{msg.decode()}` diploma here in few simple steps.")
        cipher = input("Choose signing-suit (nist384p-sha384,nist256p-sha256): ")
        if '-' not in cipher:
            print('Bad signing-suit format must be `curve`-`hash`')
            continue
        curve, _, hashFn = cipher.partition('-')
        if curve not in curves:
            print(f'Unrecognized curve `{curve}`')
            continue
        if hashFn not in hashes:
            print(f'Unrecognized hash `{hashFn}`')
            continue
        print(f"Curve: {curve}")
        print(f"Hash: {hashFn}")
        sk = sk2 if curve == 'nist256p' else sk3
        hFn = hashes[hashFn]
        order = sk2.curve.order if hashFn == 'sha256' else sk3.curve.order
        print(f"Order: {order}")
        pow = PoW(hashFn=hFn)
        nonce, tag = pow.generate()
        print("Prove you are worthy for the diploma answer the following question:")
        print(f"\t{hashFn}(???? | {nonce.decode()}) == {tag.hex()}")
        print("\t ???? is: ", end="")

        r, s = sk.sign(msg, hashfunc=hFn, sigencode=lambda r,s,_: (r,s), k=randrange(order))
        
        secret = input("")

        if pow.validate(secret.encode(), tag):
            print(f"Here is your signature: ({r}, {s})")
        else:
            print("Your proof is wrong, sorry.")

if __name__ == "__main__":
   try:
      main()
   except:
      print("Some error occured")
