#!/usr/bin/env python3

import aes
import base64, binascii
import os
import contextlib

KEY = b'scrt_fd$6aB72@aQ'
assert(len(KEY) == 16)
FLAG = b"ICHSA_CTF{Wh0_n3eds_c0mpliC4t3d_m4th_Wh3n_u_HaVe_CPA}"

if __name__ == "__main__":
    aes_cipher = aes.AES(KEY)

    with open(os.devnull, 'w') as f:
        with contextlib.redirect_stdout(f):
            encrypted_flag = aes_cipher.encrypt_ecb(FLAG)

    print("The following base64-encoded ciphertext is encrypted with aes-128-ecb:")
    print(base64.b64encode(encrypted_flag).decode("ascii"))
    print("\nYou are allowed to encrypt any input using the same key.\n")

    while True:
        try:
            usr_input = input("Enter base64-encoded input to be encrypted: ")
            decoded_input = base64.b64decode(usr_input)
            encrypted = aes_cipher.encrypt_ecb(decoded_input)
            print(f"Encrypted result:\n{base64.b64encode(encrypted).decode('ascii')}")
        except binascii.Error:
            print("Error decoding input as base64")
        except Exception:
            print("Error: Please contact admin")

