# May the Fault

It's seems the damn Empire got more serious about security, who could have predicated that?

The last mission was a total failure the team barely got back in one piece. I have interviewed the team's Astromech as per High-Command orders. It turns out that not only the Empire rotated their keys as usual, they apparently replaced the defective SonyTech's authorization-code-signer module! That must have cost them a lot, well they have the credits and manpower to do this - sorry not important right now.

High-Command is worried that the Alliance is in for a very rough time ahead. They gave me an order to find a solution to our cyber-predicament but also told me to keep quite so moral would stay high.

Good news! A Fulcrum agent got his hands on the new model - months ego! I got it two hours ego, it is marked as RFC6979 and looks like a retro fitted Coder-6979 from the Clone-Wars area, they used P-384-SHA-384 module internally if I remember corretly. It seems to give deterministic signatures as the pervious model but it is not vulnerable as the pervious model.

Unfortunately while I was having a meal in the mess hall to eat and ponder on solving our dire cyber-situation the model was destroyed by a couple of stray blaster blots when a spy was apprehended. The goodish news? The logs of my automatic testing survived! Maybe you can help me squeeze another win against the evil Empire? 

    P = (25810373618626141068872383934633639562675052062933776269205895362253154274341644402807454205459487262726974357665020, 24357870088507475796845641257845521098466543594714009079848834364869236334804539426175205689356092520560135730359002)
    ....
    'Farm boy is an idiot', ...
    'Laser brain is in love', ...
    'Statment: Meatbags are stupid!', ...
    ....
    'May the fault be with you!', (8245945886195951374860920029986293637583746593996457322629262648187909239846312581563121826358885064690666865260237, 31709420096828252236131857909178767350353072326826086592809322541080038025213253547717164226059590586808131398061671)
    'May theWADF#@R!~~~
    'May the fault be with you!', (18813546215615899255563459111578090795838253408639586167966357680985201554425827594694842156815740115835332333078796, 9411621360467604070357207160843017513215825182716283428145971367702468622706979221238375886280121963626141272166256)

![May the fault be with you](may_the_fault.jpg)
![I'm one with the fault and the fault is with me.](one_with_the_fault.jpg)



Author: [David Hai Gootvilig](https://twitter.com/_d18g_)
