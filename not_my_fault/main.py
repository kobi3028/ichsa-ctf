#!/usr/bin/env python3

import base64, binascii
from Crypto import Random
from Crypto.Random import random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto.Util.number import bytes_to_long, long_to_bytes

KEY_LEN = 16 # AES-128
KEY_LEN_BITS = KEY_LEN * 8
FLAG = b"ICHSA_CTF{It_miGh7_b3_my_f4ul7_aFt3r_4ll}"
IV = Random.new().read( AES.block_size )

def encrypt(raw, key, iv):
    raw = pad(raw, AES.block_size)
    cipher = AES.new( key, AES.MODE_CBC, iv )
    return cipher.encrypt( raw )

def clear_bit(value, offset):
    mask = ~(1 << offset)
    return(value & mask)

if __name__ == "__main__":
    key = Random.new().read(KEY_LEN)
    fault_order = list(range(KEY_LEN_BITS))
    random.shuffle(fault_order)

    print("Can you decrypt the following base64-encoded aes-128-cbc encrypted ciphertext?")
    print(base64.b64encode(encrypt(FLAG, key, IV)).decode("ascii"))
    print("Here's the IV:")
    print(base64.b64encode(IV).decode("ascii"))
    print("\n\n")

    fault_index = 0
    while True:
        try:
            usr_input = input("Enter base64-encoded input to be encrypted: ")
            decoded_input = base64.b64decode(usr_input)

            print("🗲")
            if fault_index < len(fault_order):
                key = long_to_bytes(clear_bit(bytes_to_long(key), fault_order[fault_index]), KEY_LEN)
                fault_index += 1
            else:
                assert(key == b'\x00' * KEY_LEN)

            encrypted = encrypt(decoded_input, key, IV)
            print(f"Encrypted result:\n{base64.b64encode(encrypted).decode('ascii')}")
        except binascii.Error:
            print("Error decoding input as base64")
        except Exception:
            print("Error: Please contact admin")


