
from pwn import *
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from Crypto.Util.number import bytes_to_long, long_to_bytes
import itertools

KEY_LEN = 16 # AES-128
KEY_LEN_BITS = KEY_LEN * 8

def decrypt(key, enc, iv):
    try:
        cipher = AES.new(key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc ), AES.block_size)
    except ValueError:
        return None

def set_key_bit(key, bit):
    return long_to_bytes((bytes_to_long(key) | (1 << bit)), len(key))

#p = process("./main.py")
p = remote("not_my_fault.ichsa.ctf.today", 8013)

p.recvline()
ciphertext = b64d(p.recvline())
p.recvline()
iv = b64d(p.recvline())

log.info(f"Ciphertext: {enhex(ciphertext)}")
log.info(f"IV: {enhex(iv)}")

dummy_text = b'a'
zero_key = b'\x00' * KEY_LEN
encrypted_history = []

with log.progress("Encrypting") as progress:
    for i in itertools.count():
        progress.status(f"Iteration #{i}")
        p.sendlineafter("Enter base64-encoded input to be encrypted: ", b64e(dummy_text))

        p.recvuntil("Encrypted result:\n")
        encrypted = b64d(p.recvline(keepends = False))

        decrypted =  decrypt(zero_key, encrypted, iv)
        if (decrypted == dummy_text):
            log.info("Arrived to zero key")
            break

        encrypted_history.append(encrypted)

with log.progress("Brute forcing key") as progress:
    key = zero_key
    for phase_index, encrypted_phase in enumerate(encrypted_history[::-1]):
        progress.status(f"Phase #{phase_index}/{len(encrypted_history)}")
        for i in range(KEY_LEN_BITS):
            test_key = set_key_bit(key, i)
            if (decrypt(test_key, encrypted_phase, iv) == dummy_text):
                progress.status(f"Found bit #{i} set in key")
                key = test_key
                break


# Since fault occurs before the encryption, one last bit might or might not be set in the key
decrypted = decrypt(key, ciphertext, iv)
if decrypted == None:
    for i in range(KEY_LEN_BITS):
        test_key = set_key_bit(key, i)
        decrypted = decrypt(test_key, ciphertext, iv)
        if (decrypted != None):
            # TODO: Might rarely return false decryption
            key = test_key
            break

if decrypted != None:
    log.info(f"Found key: {enhex(key)}")
    log.success(f"Flag: {decrypt(key, ciphertext, iv)}")
else:
    log.error(f"Can't find key, best guess: {enhex(key)}")

