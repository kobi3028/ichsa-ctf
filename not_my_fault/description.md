# Not My Fault

"I have no idea how they decrypted the message! Honestly! I used really advanced encryption standards! I never gave anyone else the key! It's not my fault, I swear!"

Looks like someone is performing a fault injection attack, and every attempt zeroes a random bit in the key.

![](this_is_how_we_break_AES.gif)