from typing import List
import inspect
import abc

# --------------------------------------------------------------

# User input validation classes 

class InputValidation(metaclass=abc.ABCMeta):
   def __init__(self):
      pass

   @abc.abstractmethod
   def validate(self, i):
      pass

class InputRangeValidation(metaclass=abc.ABCMeta):
   # len_from <= len(i) < len_to
   def __init__(self, range_from : int, range_to : int, oppsite : bool = False, callback : callable = None):
      self.range_from = range_from
      self.range_to   = range_to
      self.oppsite    = oppsite
      self.callback   = callback
      self.name       = "range"

   def validate(self, i):
      if self.callback is not None:
         i = self.callback(i)
      criteria = self.range_from <= i < self.range_to
      if not self.oppsite:
         if criteria:
            return True
         else:
            raise ValueError(f'input {self.name} must comply: {self.range_from} <= {self.name} < {self.range_to}')
      else:
         if not criteria:
            return True
         else:
            raise ValueError(f'input {self.name} must comply:  {self.name} < {self.range_from} or {self.name} >= {self.range_to}')

class InputLengthValidation(InputRangeValidation):
   def __init__(self, range_from : int, range_to : int, oppsite : bool = False):
      super().__init__(range_from, range_to, oppsite, len)
      self.name = "length"

class Input10Validation(metaclass=abc.ABCMeta):
   def __init__(self):
      pass

   def validate(self, i):
      if not all(c in '01' for c in i):
         raise ValueError(f'Input characters must be 1 or 0')

# --------------------------------------------------------------

# User input classes 

class InputBase(metaclass=abc.ABCMeta):
   def __init__(self, input_validations : List[InputRangeValidation] = []):
      self.input_validations = input_validations

   def parse(self, i : str):
      return i

   def input(self, prompt : str):
      while True:
         try:
            i = self.parse(input(prompt))
            for input_validation in self.input_validations:
               input_validation.validate(i)
            break
         except Exception as ex:
            print(f"Wrong input {str(ex)}")
            continue
      return i

class InputInteger(InputBase):
   def __init__(self, input_validations : List[InputRangeValidation] = []):
      super().__init__(input_validations)

   def parse(self, i : str):
      try:
         return int(i)
      except:
         raise ValueError(f'must be integer')

class InputHexBytes(InputBase):
   def __init__(self, input_validations : List[InputRangeValidation] = []):
      super().__init__(input_validations)

   def parse(self, i : str):
      try:
         return bytes.fromhex(i)
      except:
         raise ValueError(f'must be hex bytes e.g. DEADBEEF')

# --------------------------------------------------------------

class OptionPicker(object):
   def __init__(self):
      self.options : List[dict] = []
      methods = inspect.getmembers(self, predicate=inspect.ismethod)
      methods = sorted(methods, key=lambda x: x[1].__code__.co_firstlineno)

      for func_name, func in methods:
         if not func_name.startswith('__') and '_option' in func.__dict__:
            self.options.append({
               'help'    : func._option['help'],
               'callback': func
            })
   
   def run(self):
      while True:
         for i, value in enumerate(self.options):
            print(f'{i+1}. {value["help"]}')
         print(f'{len(self.options)+1}. Exit')
         index = InputInteger([InputRangeValidation(1, len(self.options) + 2)]).input(f'Enter option index (1-{len(self.options) + 1}): ') - 1
         if index == len(self.options):
            break
         self.options[index]["callback"]()
   
   def _option(help : str):
      def wrapper(func, *args, **kwargs):
         if '_option' not in func.__dict__:
            func._option = {
               'help'    : help
            }
         return func
      return wrapper