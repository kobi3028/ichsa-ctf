Mario needs your help to get the flag!

the letters of the flag are found in the end of each level and are made of coins
(one letter in the end of each level)

But Mario dies every time that the time is up :(

It seems that the game is over for regular Super Mario
Can you help Super Mario to become Super-Super Mario??

Good luck :)

![ALT](/super_super_mario/mario.gif)

dependencies (ubuntu):
* apt install libsdl2-dev
* apt install libsdl2-image-dev
* apt install libsdl2-mixer-dev

Hint 1:
* https://github.com/jakowskidev/uMario_Jakowski

Hint 2:
* Maybe try to patch some of the abilities of Mario

Hint 3:
* Yes, you can also solve it without running the game even once.
I think it takes much more effort but do what you want :)
a flag is a flag and it's your life
